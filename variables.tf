variable "tags" {
  type = map
  default = {"created_by" = "tf" } 
}

variable "ami" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "env" {
  type = string
}
