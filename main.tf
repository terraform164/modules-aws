resource "aws_instance" "ec2-instance-example" {
  ami           = var.ami
  instance_type = var.instance_type

  tags = {
    Name = var.env
    by = var.tags["created_by"]
  }
}
